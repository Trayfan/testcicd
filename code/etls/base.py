from json import loads, dumps
from time import time

class BaseETL:
    def read_data_from_file(self, file):
        with open(file, 'r') as file:
            return file.read()
        
    def convert_data_to_json(self, data):
        return loads(data)
    
    def write_data_to_file(self, file_name, data):
        with open(file_name, "w") as file:
            file.write(data)
            
    def convert_json_to_data(self, json):
        return dumps(json)
    
    def get_timestamp(self):
        return time()
    