from etls.base import BaseETL
from etls.vendor1.settings import INBOUND_FILE
from etls.settings import INBOUND_FOLDER, STORE_FOLDER

class Vendor1Inbound(BaseETL):
    def __init__(self):
        self.file = f"{INBOUND_FOLDER}/{INBOUND_FILE}"
        self.store_file = f"{STORE_FOLDER}/{self.get_timestamp()}"
    
    def transform(self, json_data):
        new_json_data = []
        for x in json_data:
            new_json = dict(
                first_name = x["first_name"],
                last_name = x["last_name"],
                birth_date = f'{x["birth_day"]}/{x["birth_month"]}/{x["birth_age"]}'
            )
            new_json_data.append(new_json)
        return new_json_data
    
    
    def run(self):
        data = self.read_data_from_file(self.file)
        json_data = self.convert_data_to_json(data)
        new_json_data = self.transform(json_data)
        etl_data = self.convert_json_to_data(new_json_data)
        self.write_data_to_file(self.store_file, etl_data)